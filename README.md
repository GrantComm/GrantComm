# Grant Commodore

## Junior Computer Science Major at Morehouse College: interested in mobile and general application development

## Soccer Player, Fisherman, and German Language Learner

## Experience working at Google, Johns Hopkins Applied Physics Laboratory, 4Site Interactive Studios

## Knowledge in Java, C++, Dart, and Flutter

## Member of CodeHouse, NSBE, and the Morehouse Pre-Alumni Association



